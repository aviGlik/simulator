# Python STL
import os
import logging

VERSION = 0.1
# Some basic cons
CURR_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.abspath(os.path.join(CURR_DIR, os.path.pardir))
LOG_DIR = os.path.join(ROOT_DIR, 'log')

# Needed for logging vars
LOG_FORMAT = '%(asctime)s - %(name)s - %(funcName)s - %(levelname)s - %(message)s'
FILE_LOG_LEVEL = logging.DEBUG
STREAM_LOG_LEVEL = logging.DEBUG
STREAM_LOG = True

# Collection name
COLLECTION = "results"



