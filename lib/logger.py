import logging
from logging import handlers

logging.basicConfig()


def setup_logger(log_format, log_file, log_file_level, log_stream_level,
                 log_file_max_size=104857600, backup_count=10, stream_log=False):
    """
    @Summary: Logger init class
    @param log_format: Log line prefix format
    @param log_file: Path to log file
    @param log_file_level: Log file logging level
    @param log_stream_level: Stream (log to screen) logging level
    @param log_file_max_size: The max size of log file, after it will reach this size - it will be rotated
    @param backup_count: The amount of rotated log files
    """

    for handler in logging.getLogger().handlers:
        logging.getLogger().removeHandler(handler)

    logger = logging.getLogger()
    formatter = logging.Formatter(log_format)

    logger.setLevel(logging.DEBUG)

    rotate_file_handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=log_file_max_size,
                                                               backupCount=backup_count)
    rotate_file_handler.setFormatter(formatter)
    rotate_file_handler.setLevel(log_file_level)
    if stream_log:
        logger_stream_handler = logging.StreamHandler()
        logger_stream_handler.setLevel(log_stream_level)
        logger_stream_handler.setFormatter(formatter)
        logger.addHandler(logger_stream_handler)
    logger.addHandler(rotate_file_handler)
