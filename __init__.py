"""
@summary: Simulator: This program should simulate the results if we had invested in the stocks/forex like the analyzer
told us.
Author: Avi Glikman
"""

# Python STL
import logging

# Private
from simulator.conf import config
import stock_data_handler




class Simulator(object):
    """
    @Summary we dont have a desgin yet so lets leave this blank
    """
    def __init__(self, result, amount=20, loss_price=1 , first_exit_price=1, first_amount_to_sell=10, sec_exit_price=2):
        """
        @param result: a recommended stock/forex by the moudle's: Classic/Kag
        @type: dict
        @param amount: amount of stock's/forxes with "bought"
        @param loss_price: The stop command value
        @type: int
        @param first_exit_price: The first profit exit point
        @type: int
        @param first_amount_to_sell: The amount of stocks/forex to sell in the first profit exit point
        @type: int
        @param sec_exit_price: The second and last profit exit point , at this point we sell all the remaining
         stocks/forex
        @type: int
        """
        self.result = result
        self.log = logging.getLogger(__file__)
        self.latest_quote = None
        self.amount = amount
        self.price = None
        self.balance = 0
        self.loss_price = result['current_price'] - loss_price
        self.first_exit_price = result['current_price'] + first_exit_price
        self.first_amount_to_sell = first_amount_to_sell
        self.sec_exit_price = result['current_price'] + sec_exit_price

    def simoulate_market(self):
        """
        @Summary: This fucntion checks if we had made profit or not
        :return:
        """
        sold_first = False
        buy_price = self.amount * self.result["current_price"]
        self.db_handler = stock_data_handler.StockDataHandler()
        self.latest_quote = self.db_handler.get_since(self.result["symbol"], self.result["interval"],
                                                      self.result["date"])
        self.log.info("Validating finical instrument {0}".format(self.result))
        for quote in self.latest_quote:
            # This mean the stock has his loss_price and we need to exit
            if quote["low"] <= self.loss_price:
                self.balance -= self.loss_price * self.amount - buy_price
                self.price = self.result["close"] - self.loss_price
                break
            # We hit the first sell rule
            if quote["high"] >= self.first_exit_price and not sold_first:
                self.balance = self.first_exit_price*self.first_amount_to_sell
                self.log.info("We hit the first exit and made {0} for stock {1} ammount {2}  ".format(
                    self.balance, self.result["symbol"], self.amount))
                sold_first = True
                self.amount -= self.first_amount_to_sell
                # In some case we might have only 1 exit rule
                if self.sec_exit_price is None:
                    break
                continue

            if quote["high"] >= self.sec_exit_price:
                self.balance += self.sec_exit_price * self.amount
                self.log.info("We hit the sec  exit and made {0} for stock {1} ammount {2}  ".format(
                    self.balance - buy_price,
                    self.result["symbol"], self.amount))
                break
        if self.balance < 0:
            self.log.info("The stock {0} interval {1} returned a loss of {2} ".format(self.result["symbol"],
                                                                                      self.result["interval"],
                                                                                      self.calc_profit()))

        else:
            self.log.info("The stock {0} interval {1} returned a profit of {2} ".format(self.result["symbol"],
                                                                                        self.result["interval"],
                                                                                        self.calc_profit()))

    def calc_profit(self):
        """
        @Calc the profit/loss in %
        :return:
        """
        self.balance = abs(self.balance)
        return (self.balance/self.amount * 100) // self.result["current_price"]
