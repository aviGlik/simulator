"""
@summary: Simulator: This program should simulate the results if we had invested in the stocks/forex like the analyzer
told us.
Author: Avi Glikman
"""

# Python STL
import logging
import os
import datetime

# Private 
from simulator.lib import logger
from simulator.conf import config
from simulator import Simulator
import stock_data_handler


def init_logger():
    """
    @summary: Initializing class logger
    """
    log_file_name = '{0}_{1}'.format('grabber',
                                         datetime.datetime.now().strftime('%Y%m%d_%H%M%S'))
    log_file_path = os.path.join(config.LOG_DIR, log_file_name)
    logger.setup_logger(config.LOG_FORMAT, log_file_path, config.FILE_LOG_LEVEL,
                        config.STREAM_LOG_LEVEL, stream_log=config.STREAM_LOG)
    return logging.getLogger(log_file_name)


def main():
    """
    @Summary: Sample file to show how to use the simulator.
    """
    try:
        log = init_logger()
        log.info("Simulator ver {0} started ".format(config.VERSION))
        start_time = datetime.datetime.now()
        # TODO: Find a smart way to run this function .
        db_handler = stock_data_handler.StockDataHandler(collection=config.COLLECTION)
        # TODO: This is update for 22/04/2016
        sample_res = db_handler.get_latest("AEP", "1 day")
        sim = Simulator(sample_res)
        sim.simoulate_market()
        finish_time = datetime.datetime.now()
        log.info("Simulator run time info:")
        log.info(" started at {0}".format(start_time))
        log.info(" finished at {0}".format(finish_time))
        log.info(" total time {0}".format(start_time - finish_time))

    except Exception as e:
        log.exception("Got exception: {0}".format(e))

if __name__ == '__main__':
    main()
